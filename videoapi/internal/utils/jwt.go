package utils

import (
	"fmt"
	"github.com/dgrijalva/jwt-go/v4"
)

type MyCustomClaims struct {
	UserId   int64
	Username string
	jwt.StandardClaims
}

func CreateJwt(key []byte, claims *MyCustomClaims) string {
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
	ss, err := token.SignedString(key)
	if err != nil {
		fmt.Println(err)
		return ""
	}
	return ss
}
