package middleware

import (
	"errors"
	"fmt"
	"github.com/zeromicro/go-zero/rest/httpx"
	"net/http"
)

type VideoMiddleware struct {
}

func NewVideoMiddleware() *VideoMiddleware {
	return &VideoMiddleware{}
}

func (m *VideoMiddleware) Handle(next http.HandlerFunc) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		// TODO generate middleware implement function, delete after code implementation
		token := r.Header.Get("Authorization")
		fmt.Println(token)
		if token == "" {
			httpx.ErrorCtx(r.Context(), w, errors.New("{\"code\":\"202\",\"msg\":\"token已失效\"}"))
			return
		}
		// Passthrough to next handler if need
		next(w, r)
	}
}
