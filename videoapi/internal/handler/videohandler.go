package handler

import (
	"net/http"

	"Junior_two/video/videoapi/internal/logic"
	"Junior_two/video/videoapi/internal/svc"
	"Junior_two/video/videoapi/internal/types"
	"github.com/zeromicro/go-zero/rest/httpx"
)

func VideoHandler(svcCtx *svc.ServiceContext) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		var req types.Publishrequest
		if err := httpx.Parse(r, &req); err != nil {
			httpx.ErrorCtx(r.Context(), w, err)
			return
		}

		l := logic.NewVideoLogic(r.Context(), svcCtx)
		resp, err := l.Video(&req)
		if err != nil {
			httpx.ErrorCtx(r.Context(), w, err)
		} else {
			httpx.OkJsonCtx(r.Context(), w, resp)
		}
	}
}
