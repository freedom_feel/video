package handler

import (
	"net/http"

	"Junior_two/video/videoapi/internal/logic"
	"Junior_two/video/videoapi/internal/svc"
	"Junior_two/video/videoapi/internal/types"
	"github.com/zeromicro/go-zero/rest/httpx"
)

func RechargeHandler(svcCtx *svc.ServiceContext) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		var req types.Rechargerequest
		if err := httpx.Parse(r, &req); err != nil {
			httpx.ErrorCtx(r.Context(), w, err)
			return
		}

		l := logic.NewRechargeLogic(r.Context(), svcCtx)
		resp, err := l.Recharge(&req)
		if err != nil {
			httpx.ErrorCtx(r.Context(), w, err)
		} else {
			httpx.OkJsonCtx(r.Context(), w, resp)
		}
	}
}
