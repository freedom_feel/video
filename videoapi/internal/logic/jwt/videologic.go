package jwt

import (
	"Junior_two/video/videorpc/pb/videoRpc"
	"context"
	"net/http"

	"Junior_two/video/videoapi/internal/svc"
	"Junior_two/video/videoapi/internal/types"

	"github.com/zeromicro/go-zero/core/logx"
)

type VideoLogic struct {
	logx.Logger
	ctx    context.Context
	svcCtx *svc.ServiceContext
}

func NewVideoLogic(ctx context.Context, svcCtx *svc.ServiceContext) *VideoLogic {
	return &VideoLogic{
		Logger: logx.WithContext(ctx),
		ctx:    ctx,
		svcCtx: svcCtx,
	}
}

func (l *VideoLogic) Video(req *types.Publishrequest) (resp *types.Publishresponse, err error) {
	// todo: add your logic here and delete this line
	res, err := l.svcCtx.VideoServer.Video(l.ctx, &videoRpc.PublishRequest{
		Title:   req.Title,
		Content: req.Content,
		UserId:  req.UserId,
	})
	if err != nil {
		return &types.Publishresponse{
			Msg:  err.Error(),
			Code: http.StatusAccepted,
			Data: "",
		}, nil
	}
	if res.Code != http.StatusOK {
		return &types.Publishresponse{
			Msg:  res.Msg,
			Code: res.Code,
			Data: res.Data,
		}, nil
	}
	return &types.Publishresponse{
		Msg:  res.Msg,
		Code: res.Code,
		Data: res.Data,
	}, nil
}
