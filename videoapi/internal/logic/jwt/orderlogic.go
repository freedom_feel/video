package jwt

import (
	"Junior_two/video/videorpc/pb/videoRpc"
	"context"
	"github.com/astaxie/beego/validation"
	"net/http"

	"Junior_two/video/videoapi/internal/svc"
	"Junior_two/video/videoapi/internal/types"

	"github.com/zeromicro/go-zero/core/logx"
)

type OrderLogic struct {
	logx.Logger
	ctx    context.Context
	svcCtx *svc.ServiceContext
}

func NewOrderLogic(ctx context.Context, svcCtx *svc.ServiceContext) *OrderLogic {
	return &OrderLogic{
		Logger: logx.WithContext(ctx),
		ctx:    ctx,
		svcCtx: svcCtx,
	}
}

func (l *OrderLogic) Order(req *types.Orederrequest) (resp *types.Orderresponse, err error) {
	// todo: add your logic here and delete this line
	valid := validation.Validation{}
	valid.Required(req.GoodNum, "goodNum").Message("goodNum is required")
	valid.Required(req.GoodId, "goodId").Message("goodId is required")
	valid.Required(req.PayType, "goodType").Message("goodType is required")
	for _, v := range valid.Errors {
		return &types.Orderresponse{
			Msg:  v.Message,
			Code: http.StatusAccepted,
		}, nil
	}
	res, err := l.svcCtx.VideoServer.Order(l.ctx, &videoRpc.OrderRequest{
		UserId:   req.UserID,
		GoodId:   req.GoodId,
		GoodNum:  req.GoodNum,
		GoodType: req.PayType,
	})
	if err != nil {
		return &types.Orderresponse{
			Msg:  err.Error(),
			Code: http.StatusAccepted,
		}, nil
	}
	if res.Code != http.StatusOK {
		return &types.Orderresponse{
			Msg:  res.Msg,
			Code: res.Code,
		}, nil
	}
	return &types.Orderresponse{
		Msg:  res.Msg,
		Code: res.Code,
		Data: res.Data,
	}, nil
}
