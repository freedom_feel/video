package jwt

import (
	"Junior_two/video/videorpc/pb/videoRpc"
	"context"
	"github.com/astaxie/beego/validation"
	"net/http"

	"Junior_two/video/videoapi/internal/svc"
	"Junior_two/video/videoapi/internal/types"

	"github.com/zeromicro/go-zero/core/logx"
)

type RechargeLogic struct {
	logx.Logger
	ctx    context.Context
	svcCtx *svc.ServiceContext
}

func NewRechargeLogic(ctx context.Context, svcCtx *svc.ServiceContext) *RechargeLogic {
	return &RechargeLogic{
		Logger: logx.WithContext(ctx),
		ctx:    ctx,
		svcCtx: svcCtx,
	}
}

func (l *RechargeLogic) Recharge(req *types.Rechargerequest) (resp *types.Rechargeresponse, err error) {
	// todo: add your logic here and delete this line
	valid := validation.Validation{}
	valid.Required(req.UserId, "userid").Message("user_id is required")
	for _, v := range valid.Errors {
		return &types.Rechargeresponse{
			Msg:  v.Message,
			Code: http.StatusAccepted,
			Data: "",
		}, nil
	}
	res, err := l.svcCtx.VideoServer.Recharge(l.ctx, &videoRpc.RechargeRequest{
		UserId:  req.UserId,
		Balance: req.Balance,
	})
	if err != nil {
		return &types.Rechargeresponse{
			Msg:  err.Error(),
			Code: http.StatusAccepted,
			Data: "",
		}, nil
	}
	if res.Code != http.StatusOK {
		return &types.Rechargeresponse{
			Msg:  res.Msg,
			Code: res.Code,
			Data: res.Data,
		}, nil
	}
	return &types.Rechargeresponse{
		Msg:  res.Msg,
		Code: res.Code,
		Data: res.Data,
	}, nil
}
