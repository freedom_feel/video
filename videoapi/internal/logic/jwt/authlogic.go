package jwt

import (
	"Junior_two/video/videorpc/pb/videoRpc"
	"context"
	"github.com/astaxie/beego/validation"
	"net/http"

	"Junior_two/video/videoapi/internal/svc"
	"Junior_two/video/videoapi/internal/types"

	"github.com/zeromicro/go-zero/core/logx"
)

type AuthLogic struct {
	logx.Logger
	ctx    context.Context
	svcCtx *svc.ServiceContext
}

func NewAuthLogic(ctx context.Context, svcCtx *svc.ServiceContext) *AuthLogic {
	return &AuthLogic{
		Logger: logx.WithContext(ctx),
		ctx:    ctx,
		svcCtx: svcCtx,
	}
}

func (l *AuthLogic) Auth(req *types.Authrequest) (resp *types.Authresponse, err error) {
	// todo: add your logic here and delete this line
	valid := validation.Validation{}
	valid.Required(req.IdCard, "IdCard").Message("身份证不可以为空")
	valid.Required(req.RealName, "realName").Message("真实姓名不可以为空")
	if valid.HasErrors() {
		for _, v := range valid.Errors {
			return &types.Authresponse{
				Msg:  v.Message,
				Code: http.StatusAccepted,
				Data: "",
			}, nil
		}
	}
	res, err := l.svcCtx.VideoServer.Auth(l.ctx, &videoRpc.AuthRequest{
		UserId:   req.UserId,
		IdCard:   req.IdCard,
		RealName: req.RealName,
	})
	if err != nil {
		return &types.Authresponse{
			Msg:  err.Error(),
			Code: http.StatusAccepted,
			Data: "",
		}, nil
	}
	if res.Code != http.StatusOK {
		return &types.Authresponse{
			Msg:  res.Msg,
			Code: res.Code,
			Data: res.Data,
		}, nil
	}
	return &types.Authresponse{
		Msg:  res.Msg,
		Code: res.Code,
		Data: res.Data,
	}, nil
}
