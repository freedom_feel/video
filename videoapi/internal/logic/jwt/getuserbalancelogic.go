package jwt

import (
	"Junior_two/video/videorpc/pb/videoRpc"
	"context"
	"github.com/astaxie/beego/validation"
	"net/http"

	"Junior_two/video/videoapi/internal/svc"
	"Junior_two/video/videoapi/internal/types"

	"github.com/zeromicro/go-zero/core/logx"
)

type GetUserbalanceLogic struct {
	logx.Logger
	ctx    context.Context
	svcCtx *svc.ServiceContext
}

func NewGetUserbalanceLogic(ctx context.Context, svcCtx *svc.ServiceContext) *GetUserbalanceLogic {
	return &GetUserbalanceLogic{
		Logger: logx.WithContext(ctx),
		ctx:    ctx,
		svcCtx: svcCtx,
	}
}

func (l *GetUserbalanceLogic) GetUserbalance(req *types.Balancerequest) (resp *types.Balanceresponse, err error) {
	// todo: add your logic here and delete this line
	valid := validation.Validation{}
	valid.Required(req.UserId, "userid").Message("user_id is required")
	for _, v := range valid.Errors {
		return &types.Balanceresponse{
			Msg:  v.Message,
			Code: http.StatusAccepted,
			Data: "",
		}, nil
	}
	res, err := l.svcCtx.VideoServer.Balance(l.ctx, &videoRpc.BalanceRequest{
		UserId: req.UserId,
	})
	if err != nil {
		return &types.Balanceresponse{
			Msg:  err.Error(),
			Code: http.StatusAccepted,
			Data: "",
		}, nil
	}
	if res.Code != http.StatusOK {
		return &types.Balanceresponse{
			Msg:  res.Msg,
			Code: res.Code,
			Data: res.Data,
		}, nil
	}
	return &types.Balanceresponse{
		Msg:  res.Msg,
		Code: res.Code,
		Data: res.Data,
	}, nil
}
