package logic

import (
	"Junior_two/video/videoapi/internal/svc"
	"Junior_two/video/videoapi/internal/types"
	"Junior_two/video/videorpc/pb/videoRpc"
	"context"
	"fmt"
	"github.com/astaxie/beego/validation"

	"github.com/zeromicro/go-zero/core/logx"
)

type DescLogic struct {
	logx.Logger
	ctx    context.Context
	svcCtx *svc.ServiceContext
}

func NewDescLogic(ctx context.Context, svcCtx *svc.ServiceContext) *DescLogic {
	return &DescLogic{
		Logger: logx.WithContext(ctx),
		ctx:    ctx,
		svcCtx: svcCtx,
	}
}

func (l *DescLogic) Desc(req *types.Descrequest) (resp *types.Descresponse, err error) {
	// todo: add your logic here and delete this line
	valid := validation.Validation{}
	valid.Required(req.HotSearchId, "hotSearch_id").Message("不可以为空")
	fmt.Println(req.HotSearchId)
	if valid.HasErrors() {
		for _, v := range valid.Errors {
			return &types.Descresponse{Data: v.Message}, nil
		}
	}
	res, err := l.svcCtx.VideoServer.Desc(l.ctx, &videoRpc.DescRequest{
		HotSearchId: req.HotSearchId,
	})
	if err != nil {
		return &types.Descresponse{Data: err.Error()}, nil
	}
	return &types.Descresponse{Data: res.Data}, nil
}
