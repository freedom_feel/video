package logic

import (
	"Junior_two/video/videorpc/pb/videoRpc"
	"context"

	"Junior_two/video/videoapi/internal/svc"
	"Junior_two/video/videoapi/internal/types"

	"github.com/zeromicro/go-zero/core/logx"
)

type ListLogic struct {
	logx.Logger
	ctx    context.Context
	svcCtx *svc.ServiceContext
}

func NewListLogic(ctx context.Context, svcCtx *svc.ServiceContext) *ListLogic {
	return &ListLogic{
		Logger: logx.WithContext(ctx),
		ctx:    ctx,
		svcCtx: svcCtx,
	}
}

func (l *ListLogic) List(req *types.Listrequest) (resp *types.Listresponse, err error) {
	// todo: add your logic here and delete this line
	res, err := l.svcCtx.VideoServer.List(l.ctx, &videoRpc.ListRequest{})
	if err != nil {
		return &types.Listresponse{Data: ""}, nil
	}
	return &types.Listresponse{Data: res.Data}, nil
}
