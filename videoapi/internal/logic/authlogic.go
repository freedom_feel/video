package logic

import (
	"context"

	"Junior_two/video/videoapi/internal/svc"
	"Junior_two/video/videoapi/internal/types"

	"github.com/zeromicro/go-zero/core/logx"
)

type AuthLogic struct {
	logx.Logger
	ctx    context.Context
	svcCtx *svc.ServiceContext
}

func NewAuthLogic(ctx context.Context, svcCtx *svc.ServiceContext) *AuthLogic {
	return &AuthLogic{
		Logger: logx.WithContext(ctx),
		ctx:    ctx,
		svcCtx: svcCtx,
	}
}

func (l *AuthLogic) Auth(req *types.Authrequest) (resp *types.Authresponse, err error) {
	// todo: add your logic here and delete this line

	return
}
