package logic

import (
	"Junior_two/video/videorpc/pb/videoRpc"
	"context"
	"fmt"
	"net/http"

	"Junior_two/video/videoapi/internal/svc"
	"Junior_two/video/videoapi/internal/types"

	"github.com/zeromicro/go-zero/core/logx"
)

type SearchLogic struct {
	logx.Logger
	ctx    context.Context
	svcCtx *svc.ServiceContext
}

func NewSearchLogic(ctx context.Context, svcCtx *svc.ServiceContext) *SearchLogic {
	return &SearchLogic{
		Logger: logx.WithContext(ctx),
		ctx:    ctx,
		svcCtx: svcCtx,
	}
}

func (l *SearchLogic) Search(req *types.Searchrequest) (resp *types.Searchresponse, err error) {
	// todo: add your logic here and delete this line
	fmt.Println(561254231)
	res, err := l.svcCtx.VideoServer.Search(l.ctx, &videoRpc.SearchRequest{Search: req.Search})
	if err != nil {
		return &types.Searchresponse{
			Msg:  err.Error(),
			Code: http.StatusConflict,
			Data: "",
		}, nil
	}
	if res.Code != http.StatusOK {
		return &types.Searchresponse{
			Msg:  res.Msg,
			Code: res.Code,
			Data: "",
		}, nil
	}
	return &types.Searchresponse{
		Msg:  res.Msg,
		Code: res.Code,
		Data: res.Data,
	}, nil
}
