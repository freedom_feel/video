package svc

import (
	"Junior_two/video/videoapi/internal/config"
	"Junior_two/video/videoapi/internal/middleware"
	"Junior_two/video/videorpc/videorpcclient"
	"github.com/zeromicro/go-zero/rest"
	"github.com/zeromicro/go-zero/zrpc"
)

type ServiceContext struct {
	Config          config.Config
	VideoServer     videorpcclient.VideoRpc
	VideoMiddleware rest.Middleware
}

func NewServiceContext(c config.Config) *ServiceContext {
	return &ServiceContext{
		Config:          c,
		VideoServer:     videorpcclient.NewVideoRpc(zrpc.MustNewClient(c.VideoServer)),
		VideoMiddleware: middleware.NewVideoMiddleware().Handle,
	}
}
