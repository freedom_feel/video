package logic

import (
	"Junior_two/video/videorpc/internal/model"
	"Junior_two/video/videorpc/internal/svc"
	"Junior_two/video/videorpc/internal/utils"
	"Junior_two/video/videorpc/pb/videoRpc"
	"context"
	"github.com/zeromicro/go-zero/core/logx"
	"net/http"
)

type VideoLogic struct {
	ctx    context.Context
	svcCtx *svc.ServiceContext
	logx.Logger
}

func NewVideoLogic(ctx context.Context, svcCtx *svc.ServiceContext) *VideoLogic {
	return &VideoLogic{
		ctx:    ctx,
		svcCtx: svcCtx,
		Logger: logx.WithContext(ctx),
	}
}

func (l *VideoLogic) Video(in *videoRpc.PublishRequest) (*videoRpc.PublishResponse, error) {
	// todo: add your logic here and delete this line
	//查看用户是否实名认证
	m := model.GetUserData(in.UserId)
	if m.AuthStatus != 1 {
		return &videoRpc.PublishResponse{
			Msg:  "请先实名认证",
			Code: http.StatusAccepted,
			Data: "",
		}, nil
	}
	//发布视频 就是添加一条数据
	if !model.PublishVideo(&model.HotSearch{
		Title:   in.Title,
		Content: in.Content,
	}) {
		return &videoRpc.PublishResponse{
			Msg:  "上传失败",
			Code: http.StatusAccepted,
			Data: "",
		}, nil
	}
	//获取 发布的数据 进行 redis缓存储存
	h := model.GetData(in.Title)
	key := "video:"
	if !utils.LPush(key, model.HotSearch{
		Id:      h.Id,
		Title:   h.Title,
		Heat:    h.Heat,
		Content: h.Content,
	}) {
		return &videoRpc.PublishResponse{
			Msg:  "redis 存入失败",
			Code: http.StatusAccepted,
			Data: "",
		}, nil
	}
	return &videoRpc.PublishResponse{
		Msg:  "ok",
		Code: http.StatusOK,
		Data: in.Content,
	}, nil
}
