package logic

import (
	"Junior_two/video/videorpc/internal/model"
	"Junior_two/video/videorpc/internal/utils"
	"context"
	"encoding/json"
	"fmt"
	"time"

	"Junior_two/video/videorpc/internal/svc"
	"Junior_two/video/videorpc/pb/videoRpc"

	"github.com/zeromicro/go-zero/core/logx"
)

type SearchLogic struct {
	ctx    context.Context
	svcCtx *svc.ServiceContext
	logx.Logger
}

func NewSearchLogic(ctx context.Context, svcCtx *svc.ServiceContext) *SearchLogic {
	return &SearchLogic{
		ctx:    ctx,
		svcCtx: svcCtx,
		Logger: logx.WithContext(ctx),
	}
}

func (l *SearchLogic) Search(in *videoRpc.SearchRequest) (*videoRpc.SearchResponse, error) {
	// todo: add your logic here and delete this line
	//通过搜索内容 进行查询
	h := model.SearchData(in.Search)
	key := "search:" + time.Now().Format(time.DateTime)
	//查询成功之后 进行redis储存
	if !utils.SetLPush(key, in.Search) {
		return &videoRpc.SearchResponse{
			Msg:  "搜索失败",
			Code: 202,
			Data: "",
		}, nil
	}
	fmt.Println(h)
	//将搜索到的内容进行序列化 输出
	jsh, _ := json.Marshal(h)
	fmt.Println(jsh)
	return &videoRpc.SearchResponse{
		Msg:  "搜索成功",
		Code: 200,
		Data: string(jsh),
	}, nil
}
