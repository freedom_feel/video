package logic

import (
	"Junior_two/video/videorpc/internal/model"
	"Junior_two/video/videorpc/internal/utils"
	"context"
	"encoding/json"
	"github.com/astaxie/beego/logs"
	"net/http"
	"strconv"

	"Junior_two/video/videorpc/internal/svc"
	"Junior_two/video/videorpc/pb/videoRpc"

	"github.com/zeromicro/go-zero/core/logx"
)

type AuthLogic struct {
	ctx    context.Context
	svcCtx *svc.ServiceContext
	logx.Logger
}

func NewAuthLogic(ctx context.Context, svcCtx *svc.ServiceContext) *AuthLogic {
	return &AuthLogic{
		ctx:    ctx,
		svcCtx: svcCtx,
		Logger: logx.WithContext(ctx),
	}
}

// Auth 实名认证的 接口
func (l *AuthLogic) Auth(in *videoRpc.AuthRequest) (*videoRpc.AuthResponse, error) {
	// todo: add your logic here and delete this line
	//第三方实名认证接口调用
	m, err := utils.Auth(in.RealName, in.IdCard)
	if err != nil {
		return &videoRpc.AuthResponse{
			Code: http.StatusConflict,
			Msg:  err.Error(),
			Data: "",
		}, nil
	}
	logs.Info(m)
	intCode, _ := strconv.Atoi(m["code"].(string))
	if m["code"] != "0" {
		return &videoRpc.AuthResponse{
			Msg:  m["message"].(string),
			Code: int64(intCode),
		}, nil
	}
	//实名认证成功后 修改用户状态 完成实名认证
	err = model.UpdateStatus(in.UserId)
	if err != nil {
		return &videoRpc.AuthResponse{
			Msg:  err.Error(),
			Code: http.StatusAccepted,
		}, nil
	}
	//可以将实名认证的结果返回出去
	jsM, _ := json.Marshal(m)
	return &videoRpc.AuthResponse{
		Msg:  m["message"].(string),
		Code: int64(intCode),
		Data: string(jsM),
	}, nil
}
