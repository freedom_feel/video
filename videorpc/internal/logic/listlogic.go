package logic

import (
	"Junior_two/video/videorpc/internal/model"
	"Junior_two/video/videorpc/internal/svc"
	"Junior_two/video/videorpc/pb/videoRpc"
	"context"
	"encoding/json"
	"github.com/zeromicro/go-zero/core/logx"
)

type ListLogic struct {
	ctx    context.Context
	svcCtx *svc.ServiceContext
	logx.Logger
}

func NewListLogic(ctx context.Context, svcCtx *svc.ServiceContext) *ListLogic {
	return &ListLogic{
		ctx:    ctx,
		svcCtx: svcCtx,
		Logger: logx.WithContext(ctx),
	}
}

func (l *ListLogic) List(in *videoRpc.ListRequest) (*videoRpc.ListResponse, error) {
	// todo: add your logic here and delete this line
	//得到 热搜的视频数据
	//这里感觉没有分页的必要 如果想了解一点 请访问  https://gitee.com/wm_51/subject/blob/master/goods/goodsrpc/model/mysql.go
	h := model.GetHotData()
	//直接将结构体序列化 进行返回
	jsH, _ := json.Marshal(h)
	return &videoRpc.ListResponse{
		Data: string(jsH),
	}, nil
}
