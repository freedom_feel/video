package logic

import (
	"context"
	"net/http"
	"strconv"

	"Junior_two/video/videorpc/internal/model"
	"Junior_two/video/videorpc/internal/svc"
	"Junior_two/video/videorpc/pb/videoRpc"

	"github.com/zeromicro/go-zero/core/logx"
)

type BalanceLogic struct {
	ctx    context.Context
	svcCtx *svc.ServiceContext
	logx.Logger
}

func NewBalanceLogic(ctx context.Context, svcCtx *svc.ServiceContext) *BalanceLogic {
	return &BalanceLogic{
		ctx:    ctx,
		svcCtx: svcCtx,
		Logger: logx.WithContext(ctx),
	}
}

// Balance 查询用余额
func (l *BalanceLogic) Balance(in *videoRpc.BalanceRequest) (*videoRpc.BalanceResponse, error) {
	// todo: add your logic here and delete this line
	//查询用户的数据 最主要的是余额
	m := model.GetUserData(in.UserId)
	f := strconv.FormatFloat(m.UserBalance, 'f', 2, 64)
	return &videoRpc.BalanceResponse{
		Code: http.StatusOK,
		Msg:  "查询成功",
		Data: f,
	}, nil
}
