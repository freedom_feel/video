package logic

import (
	"Junior_two/video/videorpc/internal/model"
	"context"
	"encoding/json"

	"Junior_two/video/videorpc/internal/svc"
	"Junior_two/video/videorpc/pb/videoRpc"

	"github.com/zeromicro/go-zero/core/logx"
)

type DescLogic struct {
	ctx    context.Context
	svcCtx *svc.ServiceContext
	logx.Logger
}

func NewDescLogic(ctx context.Context, svcCtx *svc.ServiceContext) *DescLogic {
	return &DescLogic{
		ctx:    ctx,
		svcCtx: svcCtx,
		Logger: logx.WithContext(ctx),
	}
}

// Desc 热搜详情
func (l *DescLogic) Desc(in *videoRpc.DescRequest) (*videoRpc.DescResponse, error) {
	// todo: add your logic here and delete this line
	//通过视频的ID 进行查看
	h := model.DescById(in.HotSearchId)
	jsH, _ := json.Marshal(h)
	return &videoRpc.DescResponse{
		Data: string(jsH),
	}, nil
}
