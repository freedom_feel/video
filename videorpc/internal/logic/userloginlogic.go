package logic

import (
	"Junior_two/video/videorpc/internal/model"
	"context"
	"encoding/json"
	"net/http"

	"Junior_two/video/videorpc/internal/svc"
	"Junior_two/video/videorpc/pb/videoRpc"

	"github.com/zeromicro/go-zero/core/logx"
)

type UserLoginLogic struct {
	ctx    context.Context
	svcCtx *svc.ServiceContext
	logx.Logger
}

func NewUserLoginLogic(ctx context.Context, svcCtx *svc.ServiceContext) *UserLoginLogic {
	return &UserLoginLogic{
		ctx:    ctx,
		svcCtx: svcCtx,
		Logger: logx.WithContext(ctx),
	}
}

// UserLogin 用户登录
func (l *UserLoginLogic) UserLogin(in *videoRpc.LoginRequest) (*videoRpc.LoginResponse, error) {
	// todo: add your logic here and delete this line
	//登录 没什么 好说的了 咱们都敲了怎么多遍了  这里就不多做阐述了
	u, e := model.UserLogin(in.Username, in.Password)
	if e != nil {
		return &videoRpc.LoginResponse{
			Msg:  e.Error(),
			Code: http.StatusAlreadyReported,
			Data: "",
		}, nil
	}
	jsU, _ := json.Marshal(u)
	return &videoRpc.LoginResponse{
		Msg:  "登陆成功",
		Code: http.StatusOK,
		Data: string(jsU),
	}, nil
}
