package model

import (
	"errors"
	"fmt"

	"github.com/astaxie/beego/orm"
	_ "github.com/go-sql-driver/mysql"
)

// HotSearch 热搜表
type HotSearch struct {
	Id      int64
	Title   string
	Heat    int64
	Content string
}

// User 用户表
type User struct {
	Id          int64
	Username    string
	Password    string
	Mobile      string
	Email       string
	AuthStatus  int
	UserBalance float64
	UserPhoto   string
	CreateAt    string
	DeleteAt    string
	UpdateAt    string
}

// Order 订单表
type Order struct {
	Id          int64 `orm:"auto"`
	UserId      int64
	GoodsId     int64
	GoodsNum    int64
	TotalAmount float64
	Status      int64
	PayType     string
	ShipAddress string
	CreatedAt   string
	DeletedAt   string
	UpdatedAt   string
}

// Goods 商品表
type Goods struct {
	Id             int64
	GoodsName      string
	GoodsPrice     float64
	GoodsInventory int64
	GoodsTypeId    int64
	SoldNum        int64
	IsHot          int64
	img            string
	CreatedAt      string
	DeletedAt      string
	UpdatedAt      string
}

var o orm.Ormer

func init() {
	orm.RegisterDriver("mysql", orm.DRMySQL)
	orm.RegisterDataBase("default", "mysql", "root:root@tcp(laojiequ.cn:3306)/WM")
	orm.RegisterModel(new(HotSearch), new(User), new(Order), new(Goods))
	orm.RunSyncdb("default", false, true)
	orm.Debug = true
	o = orm.NewOrm()
}

// GetHotData 获取热搜的总数据
func GetHotData() []HotSearch {
	var hS []HotSearch
	_, err := o.QueryTable("hot_search").OrderBy("-heat").All(&hS)
	if err != nil {
		fmt.Println(err)
		return []HotSearch{}
	}
	return hS
}

// DescById 通过id获取数据 详情
func DescById(HotSearchId int64) HotSearch {
	h := HotSearch{
		Id: HotSearchId,
	}
	err := o.Read(&h, "id")
	h.Heat += 1
	o.Update(&h)
	if err != nil {
		fmt.Println(err)
		return HotSearch{}
	}
	fmt.Println(h)
	return h
}

// SearchData 得到搜索的数据
func SearchData(search string) HotSearch {
	var h HotSearch
	fmt.Println(search)
	//模糊查询  orm 文档里面的 高级查询有 地址：http://topgoer.com/beego%E6%A1%86%E6%9E%B6/beego%E7%9A%84mvc%E6%9E%B6%E6%9E%84%E4%BB%8B%E7%BB%8D/model%E8%AE%BE%E8%AE%A1/%E9%AB%98%E7%BA%A7%E6%9F%A5%E8%AF%A2.html
	_, err := o.QueryTable("hot_search").Filter("title__icontains", search).All(&h)
	//fmt.Println(h)
	if err != nil {
		fmt.Println(err)
		//fmt.Println(46546466)
		return HotSearch{}
	}

	h.Heat += 1

	o.Update(&h)
	return h
}

// PublishVideo 发布视频 添加数据
func PublishVideo(v *HotSearch) bool {
	_, err := o.Insert(v)
	if err != nil {
		fmt.Println(err)
		return false
	}
	v.Heat += 1
	o.Update(v)
	return true
}

// GetData 得到发布的总数据
func GetData(title string) HotSearch {
	h := HotSearch{
		Title: title,
	}
	err := o.Read(&h, "title")
	if err != nil {
		fmt.Println(err)
		return HotSearch{}
	}
	return h
}

// GetUserData 得到用户的总数据
func GetUserData(userId int64) User {
	u := User{
		Id: userId,
	}
	err := o.Read(&u, "id")
	if err != nil {
		return User{}
	}
	return u
}

// UpdateStatus 修改用户的实名认证的状态
func UpdateStatus(userId int64) error {
	u := User{
		Id:         userId,
		AuthStatus: 1,
	}
	_, err := o.Update(&u, "auth_status")
	if err != nil {
		return err
	}
	return nil
}

// UpdateBalance 通过支付宝充值快币
func UpdateBalance(u *User) error {
	_, err := o.Update(u)
	if err != nil {
		return err
	}
	return nil
}

// UserLogin 登录
func UserLogin(username, password string) (User, error) {
	u := User{
		Username: username,
	}
	err := o.Read(&u, "username")
	if err != nil {
		fmt.Println(err)
		return User{}, errors.New("用户名不存在，请先注册")
	}
	if u.Password != password {
		return User{}, errors.New("密码错误")
	}
	return u, nil
}

// GetGoodsDataByGoodId 通过商品ID获取商品数据 进行购买 主要是单价
func GetGoodsDataByGoodId(goodId int64) Goods {
	g := Goods{
		Id: goodId,
	}
	err := o.Read(&g, "id")
	if err != nil {
		fmt.Println(err)
		return Goods{}
	}
	return g
}

// CreateOrder 创建订单 想订单表中增加数据
func CreateOrder(order *Order) bool {
	_, err := o.Insert(order)
	if err != nil {
		fmt.Println(err)
		return false
	}
	return true
}

// GoodNumReduce 减少相应的商品库存
func GoodNumReduce(id, num int64) error {
	g := Goods{
		Id: id,
	}
	o.Read(&g, "id")
	if g.GoodsInventory < num {
		errors.New("库存不足")
	}
	g.GoodsInventory -= num
	_, err := o.Update(&g)
	if err != nil {
		fmt.Println(err)
		return err
	}
	return nil
}

// UserBalanceReduce 修改用户余额
func UserBalanceReduce(id int64, totalAmount float64) error {
	u := User{
		Id: id,
	}
	o.Read(&u, "id")
	if u.UserBalance < totalAmount {
		return errors.New("余额不足")
	}
	u.UserBalance -= totalAmount
	_, err := o.Update(&u)
	if err != nil {
		fmt.Println(err)
		return err
	}
	return nil
}
