package utils

import (
	"Junior_two/video/videorpc/internal/model"
	"encoding/json"
	"fmt"
	"github.com/go-redis/redis"
)

var redisClient *redis.Client

func init() {
	redisClient = redis.NewClient(&redis.Options{
		Addr:     "127.0.0.1:6379",
		Password: "",
		DB:       1,
	})
}

func SetLPush(key string, search string) bool {
	_, err := redisClient.LPush(key, search).Result()
	fmt.Println("================================================================")
	if err != nil {
		fmt.Println(err)
		return false
	}
	return true
}

func LPush(key string, h model.HotSearch) bool {
	jsH, _ := json.Marshal(h)
	_, err := redisClient.LPush(key, jsH).Result()
	if err != nil {
		fmt.Println(err)
		return false
	}
	return true
}
