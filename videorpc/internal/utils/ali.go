package utils

import (
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"strings"
)

var (
	AppCode = "27e94c2b63c14c2d95cc688705bf0f3f"
	Url     = "http://eid.shumaidata.com/eid/check"
)

func Auth(realName, idCard string) (map[string]interface{}, error) {
	//创建一个http客户端
	client := http.Client{}
	//写一个请求参数
	body := "name=" + realName + "&idcard=" + idCard
	fmt.Println(body)
	req, err := http.NewRequest("POST", Url, strings.NewReader(body))

	if err != nil {
		fmt.Println(err)
		return nil, err
	}
	//设置 请求格式 和请求头
	req.Header.Set("Content-Type", "application/x-www-form-urlencoded")

	req.Header.Set("Authorization", "APPCODE "+AppCode)

	res, err := client.Do(req)
	fmt.Println(res)
	defer res.Body.Close()
	//读取数据
	b, err := io.ReadAll(res.Body)

	if err != nil {
		return nil, err
	}
	fmt.Println(string(b))
	var m map[string]interface{}
	json.Unmarshal(b, &m)

	return m, nil
}
